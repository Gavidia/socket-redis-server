const mongoose = require('mongoose');

const machineSchema = new mongoose.Schema({
  macA: String,
  cpuLoad: Number,
  freeMem: Number,
  totalMem: Number,
  usedMem: Number,
  memUseage: Number,
  osType: String,
  upTime: Number,
  cpuModel: String,
  numCores: Number,
  cpuSpeed: Number,
});

const Machine = mongoose.model('Machine',machineSchema);

module.exports = Machine;

