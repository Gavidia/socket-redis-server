const mongoose = require('mongoose');
mongoose.connect('mongodb://127.0.0.1:27017/perfData',{useNewUrlParser: true});
const {Machine} = require('./models/Machine');

function socketMain(io, socket){

  let macA;
  //a machine has connected, check to see if it`s new.
  // if it is, add it!

  socket.on('initPerfData',(data) => {
    macA = data.macA;
  })

  socket.on('perfData',((data)=> {
    console.log(data);
  }))
}

module.exports = socketMain;