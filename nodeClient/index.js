// The node program that captures local perfomance data.
// and sends it up to the socket.io server
// Req:
// - farmhash
// - socket.io-client

const os = require('os');
const io = require('socket.io-client');
let socket = io('http://127.0.0.1:8181');

socket.on('connect',() => {

  const nI = os.networkInterfaces();
  let macA;

  for(let key in nI){
    if(nI[key][0].internal){
      macA = nI[key][0].mac;
      break;
    }
  }

  perfomanceData().then((allPerfomanceData) => {
    console.log("AQUIIIIIIIIIII")
    allPerfomanceData.macA = macA;
    console.log(allPerfomanceData);
    socket.emit('initPerfData',allPerfomanceData);
  });

  //start sending over data on interval
  let perfDataInterval = setInterval(() => {
    perfomanceData().then((allPerfomanceData)=>{
      socket.emit('perfData', allPerfomanceData);
    });
  }, 1000);

  socket.on('disconnect',() => {
    clearInterval(perfDataInterval);
  })
 })

function perfomanceData(){
  return new Promise(async (resolve,reject) => {
    const cpus = os.cpus();
    
    // What do you need to know from node about perfomance?
    // - CPU load (current)
    // - Memory Useage
    //   - free
    const freeMem = os.freemem();
    //   - total
    const totalMem = os.totalmem();
    const usedMem = totalMem - freeMem;
    const memUseage = Math.floor(usedMem/totalMem*100)/100;
    // - OS type
    const osType = os.type() == 'Darwin' ? 'Mac' : os.type();
    // - uptime
    const upTime = os.uptime();
    // - CPU info
    //  - Type
    const cpuModel = cpus[0].model;
    //  - Number of cores
    const numCores = cpus.length;
    //  - Clock Speed
    const cpuSpeed = cpus[0].speed;
    const cpuLoad = await getCpuLoad();

    resolve({freeMem,totalMem,usedMem,memUseage,osType,upTime,cpuModel,numCores,cpuSpeed,cpuLoad});
  });
}

function cpuAverage() {

  const cpus = os.cpus();

  let idleMs = 0;
  let totalMs = 0;

  cpus.forEach((aCore) => {

    for(type in aCore.times){
      totalMs += aCore.times[type]; 
    }
    idleMs += aCore.times.idle;
  });

  return {
    idle: idleMs / cpus.length,
    total: totalMs / cpus.length
  }
}

function getCpuLoad(){

  return new Promise((resolve,reject) => {
    const start = cpuAverage();
  
    setTimeout(() => {
      const end = cpuAverage();
  
      const idleDifference = end.idle - start.idle;
      const totalDifference = end.total - start.total;
  
      const percentageCpu = 100 - Math.floor(100 * idleDifference/totalDifference);
      resolve(percentageCpu);
    },100);
  })
}

perfomanceData().then(data => console.log(data));